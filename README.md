# FTPFetch
A small BASH shell for pulling files from my FTP site and writing them
to my local NAS using SAMBA.

This project was originally written to pull video files off cameras in my
father's condo so I could retain a historical record without having to pay 
for a cloud subscription.  I posted it here because it was a useful exercise
in writing scripts that pull data from FTP (I honestly thought I was 
going to have to write it in Expect).


## To use
* Clone this repo using git clone.
* In your project directory, create two files called .secret and another called .smbsecret.
* In .secret file, write the password for your FTP account. The Password should
be written in Base64. In Unix a quick way to do this is :
```
 echo _yourpassword_ | base64 > .secret
```
* In .smbsecret, a little different, you need the base64 equivalent of 
username%password. USe technique above to generate
* Edit the file xfer.sh and set the various variables as needed. At a minimum, 
you will need an FTP host and ftp user (sorry this version doesn't support 
anonymous FTP)
* This script is expecting to communicate via SAMBA protocol using smbclient.
You can get this by 
```
 sudo apt install smbclient
```




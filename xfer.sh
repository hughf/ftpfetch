#!/bin/bash
YEAR=$(date -d "yesterday 13:00" +%Y)
MONTH=$(date -d "yesterday 13:00" +%m)
DAY=$(date -d "yesterday13:00" +%d)
DATEFOLDER=$(date -d "yesterday 13:00" +%Y/%m/%d)
MANIFEST=./manifest.txt 
CMDFILE=./ftpcmds.txt 
FETCHEDFILES=./fetched.txt
HOST=ftp.hferguson.ca
USER=video@hferguson.ca
PWD=$(cat .secret | base64 --decode) 
SMBHOST=//syn422.local
SMBSHARE=CondoVideo
SMBUSER=$(cat .smbsecret)


echo Connecting with user ${USER}
echo Host is ${HOST} 
echo Fetching files for $YEAR\/$MONTH\/$DAY 
echo Datefolder is ${DATEFOLDER} 
ftp -inv $HOST << EOF 
user ${USER} ${PWD}  
cd ${DATEFOLDER}
ls -l $MANIFEST
bye
EOF


# We should have a manifest file now. build a new cmd
if test -f "$MANIFEST"; then
    # file into array
    fList=($(<$MANIFEST))
    
    # Create a command file for fetch and download cmds
    echo "user $USER $PWD" > $CMDFILE
    echo "binary" >> $CMDFILE
    echo "cd ${DATEFOLDER}" >> $CMDFILE 
     
    # for loop to loop through array
    # ls -l gives 9 columns. so we want fp to be the start of each
    # line
    echo "" > $FETCHEDFILES 
    for ((fp=0; fp<${#fList[@]}; fp+=9));do
        file=${fList[`expr $fp+8`]}
        if [ ${file:0:1} == '.' ]; 
        then 
            continue
        fi
        echo $file >> $FETCHEDFILES 
        echo "get ${file}" >> $CMDFILE
        echo "delete ${file}" >> $CMDFILE
    done
    rm -f $MANIFEST
    
    # Execute cmd file to download files
    ftp -i -n $HOST < $CMDFILE 
    
    echo transfer from FTP Host complete. Moving to NAS...
    # Do the transfer as a single mput
    sh ./smbxfer.sh $SMBHOST $SMBSHARE $DATEFOLDER
    
    # Loop thru files again and cleanup
    while read file; do
        rm $file
    done < $FETCHEDFILES 
    
    echo All files moved to $SMBHOST
    rm fetched.txt
    rm ftpcmds.txt
else 
  echo "No files found today"
fi

#!/bin/sh
SMBHOST=$1 
SMBSHARE=$2
SMBUSER=$(cat .smbsecret)
DATEFOLDER=$3 
echo connecting to Samba drive ${SMBHOST}/${SMBSHARE}
echo and creating or entering folder ${DATEFOLDER}

smbclient $SMBHOST/$SMBSHARE --user=$(echo $SMBUSER | base64 -d) << EOC
mkdir ${DATEFOLDER}
cd ${DATEFOLDER}
recurse
prompt
mput *.mp4
exit
EOC
